/*
 ============================================================================
 Name        : eeg_mouse_read.c
 Author      : Christoph Maier
 Version     : 0.1
 Copyright   : GNU GPLv3
 Description : from http://en.wikibooks.org/wiki/Serial_Programming/termios#open.282.29
 ============================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h> // needed for memset

int main(int argc,char** argv)
{
        struct termios tio;
        struct termios stdio;
        int tty_fd;
        fd_set rdset;

        unsigned char c='!';	// separating inbound and outbound characters
        unsigned char d='\0';	// to prevent a USB character from stopping the loop

        printf("Please start with %s /dev/ttyACM0 (for example)\n",argv[0]);
        memset(&stdio,0,sizeof(stdio));
        stdio.c_iflag=0;
        stdio.c_oflag=0;
        stdio.c_cflag=0;
        stdio.c_lflag=0;
        stdio.c_cc[VMIN]=1;
        stdio.c_cc[VTIME]=0;
        tcsetattr(STDOUT_FILENO,TCSANOW,&stdio);
        tcsetattr(STDOUT_FILENO,TCSAFLUSH,&stdio);
        fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);       // make the reads non-blocking




        memset(&tio,0,sizeof(tio));
        tio.c_iflag=0;
        tio.c_oflag=0;
        tio.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
        tio.c_lflag=0;
        tio.c_cc[VMIN]=1;
        tio.c_cc[VTIME]=5;

        tty_fd=open(argv[1], O_RDWR | O_NONBLOCK);
        cfsetospeed(&tio,B921600);            // 921600 baud
        cfsetispeed(&tio,B921600);            // 921600 baud

        cfmakeraw(&tio);	// force raw data stream from USB
        tcsetattr(tty_fd,TCSANOW,&tio);
//        write(tty_fd,&c,1);		// write '!' to start ADC
        while (c!='q')
        {
                if (read(tty_fd,&d,1)>0)        write(STDOUT_FILENO,&d,1);              // if new data is available on the serial port, print it out
                if (read(STDIN_FILENO,&c,1)>0)  write(tty_fd,&c,1);                     // if new data is available on the console, send it to the serial port
        }

        close(tty_fd);
        return EXIT_SUCCESS;
}

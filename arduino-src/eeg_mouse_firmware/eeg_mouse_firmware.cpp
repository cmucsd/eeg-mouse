// modified from: http://www.windmeadow.com/node/38

#include <Arduino.h>
#include <SPI.h>

#include "eeg_mouse_firmware.h"
#include "ads1298.h"
#include "SpiDma.h"
#include "adsCommand.h"

#define DATA_BUF_SIZE 80

#ifndef LIVE_CHANNELS_NUM
#define LIVE_CHANNELS_NUM 4
#endif

#ifdef  _VARIANT_ARDUINO_DUE_X_
#define SPI_CLOCK_DIVIDER_VAL 4
#else
// #define SPI_CLOCK_DIVIDER_VAL SPI_CLOCK_DIV4
#define SPI_CLOCK_DIVIDER_VAL SPI_CLOCK_DIV8
#endif

#ifdef _VARIANT_ARDUINO_DUE_X_
#if ARDUINO_DUE_USB_PROGRAMMING == 1
#define SERIAL_OBJ Serial
#else // default to the NATIVE port
#define SERIAL_OBJ SerialUSB
#endif
#endif

#ifndef SERIAL_OBJ
#define SERIAL_OBJ Serial
#endif

// global variables
char in_byte;
unsigned char packet_start= 0x5A;
int led_status;
unsigned long last_blink;
unsigned long blink_interval_millis;

#define LED_PIN 13
#define BLINK_INTERVAL_SETUP 100;
#define BLINK_INTERVAL_WAITING 500;
#define BLINK_INTERVAL_SENDING 2000;

// if this becomes more flexible, we may need to pass in
// the byte_buf size, but for now we are safe to skip it
void fill_sample_frame(unsigned char *byte_buf)
{
	digitalWrite(IPIN_CS, LOW);

	// read 24bits of status then 24bits for each channel
	spiRec(byte_buf, (LIVE_CHANNELS_NUM+1)*3);
	delayMicroseconds(1);
	digitalWrite(IPIN_CS, HIGH);
}

void serial_print_error(const char *msg)
{
	SERIAL_OBJ.print("[oh]");
	SERIAL_OBJ.print(msg);
	SERIAL_OBJ.print("[no]\n");
}

void wait_for_drdy(const char *msg, int interval)
{
	int i = 0;
	while (digitalRead(IPIN_DRDY) == HIGH) {
		if (i < interval) {
			continue;
		}
		i = 0;
		serial_print_error(msg);
	}
}

void blink_led(void)
{
	unsigned long now = millis();

	if ((now - last_blink) > blink_interval_millis) {
		led_status = (led_status == HIGH) ? LOW : HIGH;
		digitalWrite(LED_PIN, led_status);
		last_blink = now;
	}
}

void setup_2(void)
{
	using namespace ADS1298;
	int i;

	// initialize the USB Serial connection
	SERIAL_OBJ.begin(460800);

	// set the LED on
	pinMode(13, OUTPUT);
	digitalWrite(13, HIGH);

	pinMode(IPIN_CS, OUTPUT);
#if EEG_MOUSE_HARDWARE_VERSION == 0
	pinMode(PIN_SCLK, OUTPUT);
	pinMode(PIN_DIN, OUTPUT);
	pinMode(PIN_DOUT, INPUT);
#endif

	pinMode(PIN_CLK, OUTPUT);
	pinMode(PIN_CLKSEL, OUTPUT);
	pinMode(PIN_START, OUTPUT);
	pinMode(IPIN_RESET, OUTPUT);
	pinMode(IPIN_PWDN, OUTPUT);
	pinMode(IPIN_DRDY, INPUT);

	spiBegin(IPIN_CS);
	spiInit(MSBFIRST, SPI_MODE1, SPI_CLOCK_DIVIDER_VAL);

	//digitalWrite(IPIN_CS, LOW);
	digitalWrite(PIN_CLKSEL, HIGH);

	// Wait for 20 microseconds Oscillator to Wake Up
	delay(1);		// we'll actually wait 1 millisecond

	digitalWrite(IPIN_PWDN, HIGH);
	digitalWrite(IPIN_RESET, HIGH);


	// Wait for 33 milliseconds (we will use 100 millis)
	//  for Power-On Reset and Oscillator Start-Up
	delay(100);

	// Issue Reset Pulse,
	digitalWrite(IPIN_RESET, LOW);
	// actually only needs 1 microsecond, we'll go with milli
	delay(1);
	digitalWrite(IPIN_RESET, HIGH);
	// Wait for 18 tCLKs AKA 9 microseconds, we use 1 millisec
	delay(1);

	// Send SDATAC Command (Stop Read Data Continuously mode)
	adc_send_command(SDATAC);

	// All GPIO set to output 0x0000
	// (floating CMOS inputs can flicker on and off, creating noise)
	adc_wreg(GPIO, 0);

	// Power up the internal reference and wait for it to settle
	adc_wreg(CONFIG3,
		 RLDREF_INT | PD_RLD | PD_REFBUF | VREF_4V | CONFIG3_const);
	delay(150);

	adc_wreg(RLD_SENSP, 0x03);	// use channels 1...2
	adc_wreg(RLD_SENSN, 0x00);	// for the RLD Measurement

	// Write Certain Registers, Including Input Short
	// Set Device in HR Mode and DR = fMOD/1024
	adc_wreg(CONFIG1, LOW_POWR_16k_SPS);	// HIGH_RES_32k_SPS
	adc_wreg(CONFIG2, INT_TEST);	// generate internal test signals
	// Set the first LIVE_CHANNELS_NUM channels to input signal
	for (i = 1; i <= LIVE_CHANNELS_NUM; ++i) {
		adc_wreg(CHnSET + i, ELECTRODE_INPUT | GAIN_12X);
	}
	// Set all remaining channels to shorted inputs
	for (; i <= 8; ++i) {
		adc_wreg(CHnSET + i, SHORTED | PDn);
	}

	digitalWrite(PIN_START, HIGH);
	wait_for_drdy("waiting for DRDY in setup", 1000);

	blink_interval_millis = BLINK_INTERVAL_WAITING;
}

void check_for_ping_from_serial()
{
	using namespace ADS1298;

	if (SERIAL_OBJ.available() == 0) {
		return;
	}
	// read an available byte:
	in_byte = SERIAL_OBJ.read();

	if (in_byte != 0) {
		// Put the Device Back in Read DATA Continuous Mode
		adc_send_command(RDATAC);
		blink_interval_millis = BLINK_INTERVAL_SENDING;
	}
}

void setup(void)
{
	in_byte = 0;
	led_status = HIGH;
	last_blink = 0;
	blink_interval_millis = BLINK_INTERVAL_SETUP;
	pinMode(LED_PIN, OUTPUT);
	setup_2();
	while (!in_byte)
	{
		check_for_ping_from_serial();
	}
	digitalWrite(13, LOW);		// LED off
}

void loop(void)
{
	unsigned char byte_buf[DATA_BUF_SIZE];

//	blink_led();

	// wait for a non-zero byte as a ping from the computer
	// loop until data available
	wait_for_drdy("no data", 5000);
	fill_sample_frame(byte_buf);
	SERIAL_OBJ.write(&packet_start,1);
	SERIAL_OBJ.write(byte_buf+3, 3*LIVE_CHANNELS_NUM); // ignore status
}

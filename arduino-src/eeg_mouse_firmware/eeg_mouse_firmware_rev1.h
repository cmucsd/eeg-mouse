/* eeg_mouse_firmware_rev1.h */
#ifndef _EEG_MOUSE_FIRMWARE_REV1_H_
#define _EEG_MOUSE_FIRMWARE_REV1_H_

// IPIN_ is for pins that are inverted

// CLK (clock) is PA2 PIN 61 (through level shifter)
#define PIN_CLK 61

// CS (chip select) is SS PB0 PIN 53 (through level shifter)
#define IPIN_CS 53

// SCLK (serial clock) is SCK PB1 PIN 52 (through level shifter)
#define PIN_SCLK 52

// DIN (data in) is MOSI PB2 PIN 51 (through level shifter)
#define PIN_DIN 51

// DOUT (data out) is MISO PB3 PIN 50 (direct)
#define PIN_DOUT 50

// CLKSEL (clock select) is PC14 PIN 49 (through level shifter)
#define PIN_CLKSEL 49

// RESET is PC15 PIN 48 (through level shifter)
#define IPIN_RESET 48

// PWDN (power down) is PC16 PIN 47 (through level shifter)
#define IPIN_PWDN 47

// START is PC17 PIN 46 (through level shifter)
#define PIN_START 46

// DRDY (data ready) is PC18 PIN 45 (direct)
#define IPIN_DRDY 45

#endif /* _EEG_MOUSE_FIRMWARE_REV_1_H_ */

#!/usr/bin/python
import sys, serial
# main() function
baudrate= 1200
def main():
    # expects 1 arg - serial port string
    if(len(sys.argv) != 2):
        print 'Example usage: reset-device.py ttyACM0'
        exit(1)
    strPort = '/dev/' + sys.argv[1];
    print 'Forcing reset using 1200bps open/close on port %s' % strPort
    # open serial port
    ser = serial.Serial(strPort, baudrate)
    ser.flush()
    ser.close()
# call main
if __name__ == '__main__':
    main()
